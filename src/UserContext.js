import React from 'react'


const UserContext = React.createContext()

// Initializes a context provider
// Gives us ability to provide a specific context through a component


export const UserProvider = UserContext.Provider


export default UserContext 


	