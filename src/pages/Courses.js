import CourseCard from '../components/CourseCard'
import {useEffect, useState} from 'react'
import Loading from  '../components/Loading'


// import courses_data from '../data/courses' <---- for testing purpose only if no data coming from database

export default function Courses(){

	const [courses, setCourses] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	useEffect((isLoading) => {
		// Set the laoding state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(response => response.json())
		.then(result => {
			setCourses(
			  result.map((course) => {
				return (
					<CourseCard key={course._id} course={course}/>
					)
				})
			)
			// Sets the loading state to false
			setIsLoading(false)

		})
	}, [])

	return(
           	  (isLoading === true) ? 
           			<Loading/>
           	   :
           	   <>
           	   	{courses}
           	   </>		
	)

}