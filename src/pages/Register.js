import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import { useNavigate, Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'




export default function Register(){
	
	const {user} = useContext(UserContext)


	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	// For determining if button is disabled or not
	const [isActive, setIsActive] = useState(false)

	// Initialize useNavigate
	const navigate = useNavigate()


	function registerUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		}).then(response => response.json())
	      .then(result => {
	      	   if(result === true){
		      		Swal.fire({
			  			title: "Oops!",
			  			icon: "error",
			  			text: "Email is already in use! "
			  		})

			  	} else {
			  		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			  			method: 'POST',
						headers: {
							'Content-Type': 'application/json',
			            },
			            body: JSON.stringify({
			            	firstName: firstName,
			            	lastName: lastName,
			            	mobileNo: mobileNumber,
			            	email: email,
			            	password: password1

                        })
			  		}).then(response => response.json())
			  		  .then(result => {
			  		  	   if(result !== false){
			  		  	   	setEmail('')
							setPassword1('')
							setPassword2('')
							setFirstName('')
							setLastName('')
							setMobileNumber('')

							Swal.fire({
				  			title: "Success!",
				  			icon: "success",
				  			text: "You have successfully Registered! "
			  		        })

							navigate('/login')

			  		  	   } else {
			  		  	   	Swal.fire({
				  			title: "Oops!",
				  			icon: "error",
				  			text: "Something Went Wrong! "
			  		        })
			  		  	   }
			  		    }) 
			    }
        })
    }

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2])

	
		
	return(
		(user.id !== null ) ?
				<Navigate to="/courses"/>
		:
		<Form onSubmit={event => registerUser(event)}>
	        <Form.Group controlId="firstName">
	            <Form.Label>First Name</Form.Label>
	            <Form.Control 
	                type="text" 
	                placeholder="Enter First Name"
	                value={firstName}
	                onChange={event => setFirstName(event.target.value)}  
	                required
	            />
	        </Form.Group>

	        <Form.Group controlId="lastName">
	            <Form.Label>Last Name</Form.Label>
	            <Form.Control 
	                type="text" 
	                placeholder="Enter First Name"
	                value={lastName}
	                onChange={event => setLastName(event.target.value)}  
	                required
	            />
	        </Form.Group>

	        <Form.Group controlId="mobileNumber">
	            <Form.Label>Mobile Number</Form.Label>
	            <Form.Control 
	                type="text" 
	                placeholder="Enter Mobile Number"
	                value={mobileNumber}
	                onChange={event => setMobileNumber(event.target.value)}  
	                required
	            />
	        </Form.Group>


	        <Form.Group controlId="userEmail">
	            <Form.Label>Email address</Form.Label>
	            <Form.Control 
	                type="email" 
	                placeholder="Enter email"
	                value={email}
	                onChange={event => setEmail(event.target.value)}  
	                required
	            />
	            <Form.Text className="text-muted">
	                We'll never share your email with anyone else.
	            </Form.Text>
	        </Form.Group>

	        <Form.Group controlId="password1">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password1}
	                onChange={event => setPassword1(event.target.value)}  
	                required
	            />
	        </Form.Group>

	        <Form.Group controlId="password2">
	            <Form.Label>Verify Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
	                value={password2}
	                onChange={event => setPassword2(event.target.value)}   
	                required
	            />
	        </Form.Group>

	        {	isActive ? 
	        	<Button variant="primary" type="submit" id="submitBtn">
	        	Submit
	        	</Button>
	        	:
	        	<Button variant="primary" type="submit" id="submitBtn" disabled>
	        	Submit
	        	</Button>
	        }
	    </Form>
   )
}