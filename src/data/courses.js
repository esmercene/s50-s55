const courses_data = [
	{
		id: "wdc001",
		name: "PHP and Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi natus eos, enim quas nihil deleniti ea nisi ipsam hic sed impedit laborum pariatur quo, quos? Illo, quisquam. Tempora explicabo, dignissimos.",
		price: 40000,
		onoffer: true
	},
	{
		id: "wdc002",
		name: "Python and Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi natus eos, enim quas nihil deleniti ea nisi ipsam hic sed impedit laborum pariatur quo, quos? Illo, quisquam. Tempora explicabo, dignissimos.",
		price: 50000,
		onoffer: true
	},
	{
		id: "wdc003",
		name: "JAVA and Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi natus eos, enim quas nihil deleniti ea nisi ipsam hic sed impedit laborum pariatur quo, quos? Illo, quisquam. Tempora explicabo, dignissimos.",
		price: 70000,
		onoffer: true
	}


]


export default courses_data 
